echo 1 thread
OMP_NUM_THREADS=1 ./main 10000000 5
echo 2 threads
OMP_NUM_THREADS=2 ./main 10000000 5
echo 3 threads-------------------------
OMP_NUM_THREADS=3 ./main 10000000 5
echo 4 threads------------------------
OMP_NUM_THREADS=4 ./main 10000000 5
echo 5 threads-----------------------
OMP_NUM_THREADS=5 ./main 10000000 5
echo 6 threads-----------------------
OMP_NUM_THREADS=6 ./main 10000000 5
echo 7 threads-----------------------
OMP_NUM_THREADS=7 ./main 10000000 5
echo 8 threads-----------------------
OMP_NUM_THREADS=8 ./main 10000000 5
echo 9 threads-----------------------
OMP_NUM_THREADS=9 ./main 10000000 5
echo 10 threads-----------------------
OMP_NUM_THREADS=10 ./main 10000000 5
echo 11 threads-----------------------
OMP_NUM_THREADS=11 ./main 10000000 5
echo 12 threads-----------------------
OMP_NUM_THREADS=12 ./main 10000000 5
